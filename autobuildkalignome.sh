#!/usr/bin/bash
# Author of the automation script @nu11secur1ty https://www.nu11secur1ty.com/
# Owner Raphaël Hertzog https://gitlab.com/rhertzog
mkdir -p g0t0vanec
cd g0t0vanec
     apt install -y curl git live-build cdebootstrap
     git clone https://github.com/nu11secur1ty/Kali_Linux_Live_Build.git
     cd Kali_Linux_Live_Build/
           ./build.sh --variant gnome --verbose
     exit 0;
