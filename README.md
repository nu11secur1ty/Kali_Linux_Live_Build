# Live-build configuration for Kali ISO images

Have a look at https://docs.kali.org/development/live-build-a-custom-kali-iso
for explanations on how to use this repository.

# Direct install "Gnome"
```bash
curl -s https://raw.githubusercontent.com/nu11secur1ty/Kali_Linux_Live_Build/master/autobuildkalignome.sh | bash
```


# Direct install "KDE"
```bash
curl -s https://raw.githubusercontent.com/nu11secur1ty/Kali_Linux_Live_Build/master/autobuildkalikde.sh | bash
```


# Have Fun ;)
